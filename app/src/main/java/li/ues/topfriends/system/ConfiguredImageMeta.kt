package li.ues.topfriends.system

import java.io.Serializable

data class ConfiguredImageMeta(
    val maxFriends: Long,
    val columns: Long,
    val showRanking: Boolean,
    val threads: ArrayList<Thread>
): Serializable
