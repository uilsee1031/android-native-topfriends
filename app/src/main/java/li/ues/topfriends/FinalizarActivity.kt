package li.ues.topfriends

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.AnimationUtils
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.net.toUri
import androidx.core.text.HtmlCompat
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.Gson

import java.net.MalformedURLException
import java.net.URL
import androidx.core.os.ConfigurationCompat.getLocales
import li.ues.topfriends.system.ConfiguredImageMeta
import li.ues.topfriends.system.ImageMeta
import li.ues.topfriends.system.ImageVariables
import org.intellij.lang.annotations.Language
import java.io.BufferedWriter
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileWriter
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import kotlin.math.sign


class FinalizarActivity : GenericActivity() {
    protected fun setStatus(str: String) {
        runOnUiThread {
            val status = findViewById<View>(R.id.status) as TextView
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                status.text = Html.fromHtml(str, HtmlCompat.FROM_HTML_MODE_LEGACY)
            } else {
                status.text = Html.fromHtml(str)
            }
        }
    }

    protected fun setStatus2(str: String) {
        runOnUiThread {
            val status = findViewById<View>(R.id.status2) as TextView
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                status.text = Html.fromHtml(str, HtmlCompat.FROM_HTML_MODE_LEGACY)
            } else {
                status.text = Html.fromHtml(str)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finalizar)

        val webView = findViewById<WebView>(R.id.webView)

        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                webView.loadUrl("javascript:window.__gather();")
            }
        }

        webView.settings.userAgentString = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3833.0 Safari/537.36"

        val mAdView = findViewById<View>(R.id.adView) as AdView
        if (BuildConfig.DEBUG) {
            val adRequest = AdRequest.Builder().addTestDevice("8D246CE1209AEC041682E49E2C87AB56").build()
            mAdView.loadAd(adRequest)
        } else {
            val adRequest = AdRequest.Builder().build()
            mAdView.loadAd(adRequest)
        }

        val logo = findViewById<View>(R.id.logo) as ImageView
        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.newanim)
        logo.animation = animation

        val intent = intent

        setStatus(getString(R.string.finalizar_status1))

        val task = object : AsyncTask<Void, Void, Void>() {
            private var retries = 0
            override fun doInBackground(vararg voids: Void): Void? {
                var meta: ConfiguredImageMeta? = null
                if (intent.getSerializableExtra("data") is ConfiguredImageMeta) {
                    meta = intent.getSerializableExtra("data") as ConfiguredImageMeta
                }

                if (meta == null) {
                    runOnUiThread {
                        Toast.makeText(
                            applicationContext,
                            getString(R.string.finalizar_generic_error),
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }

                    return null
                }

                setStatus(getString(R.string.finalizar_status2))

                val bitmap = getBitmap(webView, meta!!)

                if (bitmap == null) {
                    runOnUiThread {
                        Toast.makeText(
                            applicationContext,
                            getString(R.string.finalizar_generic_error),
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }
                    return null
                } else {
                    val bStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bStream)
                    val byteArray = bStream.toByteArray()

                    val file = File(this@FinalizarActivity.cacheDir, "bitmap.jpg")

                    val os = contentResolver.openOutputStream(file.toUri())
                    if (os != null) {
                        os.write(byteArray)
                        os.flush()
                        os.close()

                        logAction("Imagem gerada")

                        val newIntent = Intent(this@FinalizarActivity, FinalizadoActivity::class.java)
                        newIntent.putExtra("bitmap", file.absolutePath);
                        startActivity(newIntent)
                        finish()
                        return null
                    }
                }

                runOnUiThread {
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.finalizar_generic_error),
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }

                return null
            }
        }

        task.execute()


    }

    fun getBitmap(w: WebView, meta: ConfiguredImageMeta): Bitmap? {
        val signal = CountDownLatch(1);
        val result = AtomicReference<Bitmap>();

        val variables = ImageVariables.fromMeta(meta).scale(1.5)

        val threadsLimited = if (meta.threads.size > variables.limit) {
            meta.threads
                .sortedByDescending { it.MessageCount }
                .slice(0 until (variables.limit).toInt())
        } else {
            meta.threads
        }

        val dpi = resources.displayMetrics.density

        val messagesTitleSingular = getString(R.string.image_messages_singular)
        val messagesTitlePlural = getString(R.string.image_messages_plural)
        val messagesTitleNone = getString(R.string.image_messages_none)

        val getMessagesTitle = { i: Long ->
            when {
                i == 1L -> messagesTitleSingular.format(i)
                i > 1L -> messagesTitlePlural.format(i)
                else -> messagesTitleNone.format(i)
            }
        }

        val htmlThreads = threadsLimited.mapIndexed { index, value ->
            var position = ""

            if (meta.showRanking) {
                position = """
                    <svg xmlns="http://www.w3.org/2000/svg" class="box-number">
                        <text x="100%" y="50%" dy=".3em" text-anchor="end" xml:space="preserve">${index + 1}</text>
                    </svg>
                """
            }

            @Language("HTML") val html = """
<div class="thread">
    <div class="box-padding">
        <div class="box">
            $position
            <div class="inner-box">
                <div class="box-picture" style="background-image: url(${value.OtherUser.ImageUrl})"></div>
                <div class="box-user-info">
                    <div class="box-name">${value.OtherUser.Name}</div>
                    <div class="box-count">${getMessagesTitle(value.MessageCount)}</div>
                </div>
            </div>
        </div>
    </div>
</div>
            """

            html
        }.joinToString("\n")

        val css = String(classLoader.getResource("style.css").openStream().readBytes(), Charsets.UTF_8)

        val headerTitle = getString(R.string.image_header)
        val footerTitle = getString(R.string.image_footer).format(
            """<a href="javascript:;">topfriends.biz</a>""",
            """<a href="javascript:;">topamig.us</a>"""
        )


        @Language("HTML") var body = """<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <style>
        $css
    </style>
</head>

<body>
<div id="measure">
    <div class="main">
        <div class="container">
            <div class="inner-container">
                <div class="header">
                    <div>
                        ${headerTitle}
                    </div>
                </div>
                <div class="thread-list">
                    $htmlThreads
                </div>
                <div class="footer">
                    <div>
                        ${footerTitle}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.__gather = function() {
      setTimeout(function() {
        var el = document.getElementById("measure");
        console.log(JSON.stringify({
          bw: document.body.clientWidth,
          bh: document.body.clientHeight,
          bw2: document.body.offsetWidth,
          bh2: document.body.offsetHeight,
          w: el.clientWidth,
          h: el.clientHeight,
          w2: el.offsetWidth,
          h2: el.offsetHeight,
          rect: el.getClientRects()[0],
          rect2: el.getBoundingClientRect()
        }, null, '\t'))
        R.done(
          el.offsetWidth,
          el.offsetHeight,
          JSON.stringify({
          })
        );
      }, 500)
    }

</script>
</body>

</html>
        """

        for (v in variables.toMap()) {
            body = body.replace("\${${v.key}}", v.value.toString())
        }

        val base64 = android.util.Base64.encodeToString(body.toByteArray(Charsets.UTF_8), android.util.Base64.DEFAULT);

        val baseUri = "data:text/html;charset=utf8;base64,$base64"


        runOnUiThread {
            w.setInitialScale(1)

            w.layout(
                0,
                0,
                (variables.targetWidth).toInt(),
                (variables.targetHeight).toInt()
            )

            w.settings.loadWithOverviewMode = true
            w.settings.useWideViewPort = true
            w.settings.javaScriptEnabled = true

            w.addJavascriptInterface(object : RenderJavaScriptInterface {
                @JavascriptInterface
                override fun done(_width: String, _height: String, a: String) {
                    try {
                        val bitmap = Bitmap.createBitmap(
                            (variables.targetWidth).toInt(),
                            (variables.targetHeight).toInt(),
                            Bitmap.Config.ARGB_8888
                        )

                        val canvas = Canvas(bitmap)
                        w.draw(canvas);

                        result.set(bitmap)
                        signal.countDown()

//                    runOnUiThread {
//                        val imageView = findViewById<AppCompatImageView>(R.id.webViewResult)
//                        imageView.setImageBitmap(bitmap)
//                    }

//                    runOnUiThread {
//                        val values = ContentValues()
//                        values.put(MediaStore.Images.Media.TITLE, getString(R.string.finalizado_visualizar))
//                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
//
//                        val uri = contentResolver.insert(
//                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                            values
//                        )
//
//                        val outstream: OutputStream?
//                        try {
//                            outstream = contentResolver.openOutputStream(uri!!)
//                            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outstream)
//                            outstream!!.close()
//                        } catch (e: Exception) {
//                            System.err.println(e.toString())
//                        }
//
//                        val view = Intent(Intent.ACTION_VIEW)
//                        view.setDataAndType(uri, "image/*")
//                        this@MainActivity.startActivity(view)
//                    }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        signal.countDown()
                    }
                }
            }, "R")
            w.loadData(base64, "text/html; charset=utf-8", "base64")
        }

        try {
            signal.await(5, TimeUnit.MINUTES)

            return result.get()
        } catch (ex: Exception){
            return null
        }
    }
}
