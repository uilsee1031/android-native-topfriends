package li.ues.topfriends

import android.widget.LinearLayout
import android.content.Context
import android.widget.TextView
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import androidx.annotation.StyleableRes
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.browser.customtabs.CustomTabsClient.getPackageName
import androidx.core.content.ContextCompat.startActivity
import io.reactivex.disposables.Disposable
import androidx.core.content.ContextCompat.startActivity




class Header(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    var globalSubscription: Disposable? = null

    init {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet) {
        View.inflate(context, R.layout.header, this)

        val update = findViewById<TextView>(R.id.app_status_needs_update)
        val feedback = findViewById<Button>(R.id.feedback)

        feedback.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/forms/d/e/1FAIpQLScXIZyHlKOFPdjKG1TyfBPyMyzGDzwUCyknnbZLMTyBYVIh4Q/viewform"))
            context.startActivity(browserIntent)
        }

        update.setOnClickListener {
            val ctx = getContext()
            // https://play.google.com/store/apps/details?id=li.ues.topfriends
            val appPackageName = ctx.packageName // getPackageName() from Context or Activity object
            try {
                ctx.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                ctx.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }
        }

        setStatus(-1)

        globalSubscription = Global.current.meta.subscribe {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)

            if (it == null) {
                setStatus(-1)
            } else {
                if (it.latestVersion == -1L) {
                    setStatus(-1)
                } else {
                    var version = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        pInfo.longVersionCode
                    } else {
                        pInfo.versionCode.toLong()
                    }

                    if (it.latestVersion > version) {
                        setStatus(1)
                    } else {
                        setStatus(0)
                    }
                }
            }
        }
    }

    override fun onViewRemoved(child: View?) {
        super.onViewRemoved(child)

        if (this.globalSubscription != null) {
            if (!this.globalSubscription!!.isDisposed) {
                this.globalSubscription!!.dispose()
            }
        }
    }

    private fun setStatus(status: Int) {
        val update = findViewById<TextView>(R.id.app_status_needs_update)
        val verifying = findViewById<TextView>(R.id.app_status_verifying)
        val ok = findViewById<TextView>(R.id.app_status_ok)
        update.visibility = View.GONE
        verifying.visibility = View.GONE
        ok.visibility = View.GONE

        when(status) {
            -1 -> verifying.visibility = View.VISIBLE
            0 -> ok.visibility = View.VISIBLE
            1 -> update.visibility = View.VISIBLE
        }
    }
}