package li.ues.topfriends

import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.gson.*
import li.ues.topfriends.system.ImageMeta
import li.ues.topfriends.system.Thread
import li.ues.topfriends.system.User
import org.json.JSONArray
import org.json.JSONException

import java.util.ArrayList
import java.util.concurrent.atomic.AtomicInteger

internal class Participants : ArrayList<User>() {
    fun from(fbid: String): User? {
        return this.filter {
            it.Id == fbid || it.FbId == fbid
        }.firstOrNull()
//        return FluentIterable.from(this)
//            .filter { input ->
//                if (input == null) {
//                    false
//                } else input.Id == fbid || input.FbId == fbid
//            }
//            .first()
//            .orNull()
    }

    @Throws(JSONException::class)
    fun from(arrayOfFbids: JSONArray): List<User> {
        val finalUsers = ArrayList<User>()
        for (i in 0 until arrayOfFbids.length()) {
            val fbid = arrayOfFbids.getString(i)

            val found = this.find {
                it.Id == fbid
            }

//            val found = Iterables.tryFind(this) { input ->
//                if (input == null) {
//                    false
//                } else input.Id == fbid
//            }.orNull()

            if (found != null) {
                finalUsers.add(found)
            }
        }

        return finalUsers
    }
}

class PreImageActivity : GenericActivity() {

    private val index = AtomicInteger(0)

    protected fun setStatus(str: String) {
        runOnUiThread {
            val status = findViewById<View>(R.id.status) as TextView
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                status.text = Html.fromHtml(str, HtmlCompat.FROM_HTML_MODE_LEGACY)
            } else {
                status.text = Html.fromHtml(str)
            }
        }
    }

    fun JsonObject.getStringMember(key: String): String? {
        try {
            if (this.isJsonNull) {
                return null
            }

            val value = this.getAsJsonPrimitive(key)

            if (value != null) {
                return value.asString
            }

            return null
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    protected fun die() {
        runOnUiThread { finish() }
    }

    // THE WRONGNESS
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pre_image)

        val loadingLogo = findViewById<View>(R.id.logo) as ImageView
        loadingLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.newanim));

        val mAdView = findViewById<View>(R.id.adView) as AdView
        if (BuildConfig.DEBUG) {
            val adRequest = AdRequest.Builder().addTestDevice("8D246CE1209AEC041682E49E2C87AB56").build()
            mAdView.loadAd(adRequest)
        } else {
            val adRequest = AdRequest.Builder().build()
            mAdView.loadAd(adRequest)
        }

        val logo = findViewById<View>(R.id.logo) as ImageView

        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.newanim)

        logo.animation = animation


        setStatus(getString(R.string.pre_image_status2))

        val task = object : AsyncTask<Void, Void, ImageMeta>() {
            var retries: Int = 0

            override fun doInBackground(vararg params: Void): ImageMeta? {
                if (retries > 3) {
                    setStatus(getString(R.string.pre_image_retries_exceeded))
                    logError("3 erros ao contactar o Facebook")

                    try {
                        java.lang.Thread.sleep(5000)
                    } catch (e: Exception) {
                    }

                    die()

                    return null
                }

                if (MainActivity.userData == null) {
                    setStatus(getString(R.string.pre_image_retries_exceeded))
                    logError("MainActivity.userData is null")

                    try {
                        java.lang.Thread.sleep(5000)
                    } catch (e: Exception) {
                    }

                    die()

                    return null
                }

                setStatus(getString(R.string.pre_image_status3))

                val newContent = Utils.newRawPostUrl(
                    "https://web.facebook.com/api/graphqlbatch/",
                    Utils.TYPE_FORM,
                    "batch_name=MessengerGraphQLThreadlistFetcher" +
                            "&fb_dtsg=" + MainActivity.userData!!.fbDtsg +
                            "&queries=%7B%22aaaa%22%3A%7B%22doc_id%22%3A%222317390158348586%22%2C%22query_params%22%3A%7B%22limit%22%3A50%2C%22before%22%3Anull%2C%22tags%22%3A%5B%22INBOX%22%5D%2C%22isWorkUser%22%3Afalse%2C%22includeDeliveryReceipts%22%3Atrue%2C%22includeSeqID%22%3Afalse%7D%7D%7D"
                )

                if (newContent == null) {
                    retries += 1
                    setStatus(String.format(getString(R.string.pre_image_status4), retries.toString()))
                    logError("Erro ao contactar o Facebook")

                    try {
                        java.lang.Thread.sleep(5000)
                    } catch (e: Exception) {
                    }

                    return doInBackground(*params)
                }

                index.set(0)

                try {
                    val gson = GsonBuilder().create()
                    val p = JsonStreamParser(newContent)
                    var obj: JsonObject? = null

                    while (p.hasNext()) {
                        val e = p.next()
                        if (e.isJsonObject) {
                            if (e.asJsonObject.has("aaaa")) {
                                obj = e.asJsonObject
                            }
                        }
                    }

                    if (obj != null) {
                        val arr = obj.getAsJsonObject("aaaa")
                            .getAsJsonObject("data")
                            .getAsJsonObject("viewer")
                            .getAsJsonObject("message_threads")
                            .getAsJsonArray("nodes")

                        val threads = ArrayList<Thread>()

                        for (_jsonThread in arr) {
                            try {
                                val jsonThread = _jsonThread.asJsonObject

                                val jsonParticipants = jsonThread
                                    .getAsJsonObject("all_participants")
                                    .getAsJsonArray("edges")

                                val otherUserId = jsonThread
                                    .getAsJsonObject("thread_key")
                                    .getStringMember("other_user_id")

                                val participants = jsonParticipants.map { input ->
                                    val obj = input!!.asJsonObject
                                        .getAsJsonObject("node")
                                        .getAsJsonObject("messaging_actor")
                                    val u = User()
                                    u.Id = obj.getStringMember("id")

                                    u.Name = obj.getStringMember("name")
                                    if (u.Name != null) {
                                        u.Name = u.Name!!.trim()
                                    }
                                    u.ShortName = obj.getStringMember("short_name")
                                    if (u.ShortName == null && u.Name != null) {
                                        u.ShortName = u.Name
                                    } else {
                                        u.ShortName = u.ShortName!!.trim()
                                    }
                                    u.ImageUrl = obj
                                        .getAsJsonObject("big_image_src")
                                        .getStringMember("uri")

//                                    val x = index.incrementAndGet()
//
//                                    u.ImageUrl = "https://placekitten.com/" + (200 + (x * 10)) +  "/" + (200 + (x * 10))
//                                    u.Name = "Gatinho #" + x
//                                    u.ShortName = "Gatinho #" + x

                                    u
                                }.toList()

                                val otherUser = participants.filter {
                                    it.Id == otherUserId
                                }.firstOrNull()

//                                val otherUser = FluentIterable.from(participants)
//                                    .filter { input -> input!!.Id == otherUserId }
//                                    .first()
//                                    .orNull()

                                if (
                                    otherUser != null
                                    && otherUserId != null
                                    && participants.size < 3
                                    && otherUser.Name != null
                                    && otherUser.Name != ""
                                ) {
                                    val t = Thread(
                                        Participants = participants,
                                        OtherUser = otherUser,
                                        OtherUserId = otherUserId,
                                        MessageCount = jsonThread.get("messages_count").asLong
                                    )
//                                    t.Participants = participants
//                                    t.OtherUser = otherUser
//                                    t.OtherUserId = otherUserId
//                                    t.MessageCount = jsonThread.get("messages_count").asLong
//
                                    threads.add(t)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                        val meta = ImageMeta(threads)
                        val intent = Intent(this@PreImageActivity, ConfigurarImagemActivity::class.java)
                        intent.putExtra("data", meta)
                        startActivity(intent)
                        finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                    retries += 1
                    setStatus(String.format(getString(R.string.pre_image_status4), retries!!.toString()))
                    logError("Erro no resultado do Facebook")
                    return doInBackground(*params)
                }

                return null
            }
        }

        task.execute()

    }

}