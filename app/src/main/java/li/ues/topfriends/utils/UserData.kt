package li.ues.topfriends.utils

import android.content.Intent

class UserData {
    var id: String? = null
    var fbDtsg: String? = null
    var name: String? = null
    var alias: String? = null
    var profilePicture: String? = null

    val isOk: Boolean
        get() = id!!.length > 2 && fbDtsg!!.isNotEmpty()

    constructor(data: Intent) {
        this.id = data.getStringExtra("id")
        if (this.id == null) {
            this.id = ""
        }

        this.fbDtsg = data.getStringExtra("fbDtsg")
        if (this.fbDtsg == null) {
            this.fbDtsg = ""
        }
    }

    constructor(id: String, fbDtsg: String) {
        this.id = id
        this.fbDtsg = fbDtsg
    }

    fun exists(): Boolean {
        return name != null && name!!.isNotEmpty()
    }
}
